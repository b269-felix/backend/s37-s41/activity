const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

// Route for checking if the user's email already exists in the database
const userController = require("../controllers/userController");

const auth = require("../auth");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details

// The "auth.verity" acts as a middleware to ensure that the user is logged in before they can enroll to a course
router.post("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

// Provides the user's ID for the getProfile controller method
userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// Route for authenticated user enrollment
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;




