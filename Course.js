// [SECTION] S37 ACTIVITY

const mongoose = require("require");

const courseSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [true, "Name is required"]
	},
	description : {
		type: String,
		description: [true, "Description is required"]
	},
	price : {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive : {
		type: Boolean,
		default: true
	},
	// isAdmin : {
	// 	type: Boolean,
	// 	default: true
	// },
	createdOn : {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}

		}
	]
});



module.exports = mongoose.model("Course", courseSchema);